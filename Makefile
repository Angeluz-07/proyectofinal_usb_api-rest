CC=gcc

LIB= -ludev -lpthread -lm -ljansson -lmicrohttpd

all: monitor_usb webserver

monitor_usb: monitor_usb.o functions.o csapp.o
	$(CC) monitor_usb.o functions.o csapp.o -o monitor_usb $(LIB)

monitor_usb.o: ./src/monitor_usb.c
	$(CC) -I . -c ./src/monitor_usb.c 

webserver: webserver.o functions.o csapp.o
	$(CC)  -I . webserver.o functions.o csapp.o -o webserver $(LIB)

webserver.o: ./src/webserver.c 
	$(CC) -w -I . -c ./src/webserver.c 

functions.o: ./src/functions.c
	$(CC)  -w -I . -c ./src/functions.c $(LIB)

csapp.o: ./lib/csapp.c
	$(CC) -O2 -Wall -I . -c ./lib/csapp.c
clean:
	rm -f *.o monitor_usb webserver ./scripts/* *.txt


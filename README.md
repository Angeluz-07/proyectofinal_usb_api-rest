Integrantes:
	Harold Aragon
	Alejandro Mena

Requerimientos:

	-Instalar librerias:
		apt-get install libudev-dev
		
		apt-get install libmicrohttpd-dev
	
		apt-get install libjansson-dev
	
	-Puerto 9999 reservado para el web server.


Uso:
	1. Compilar: make
	
	2. Ejecutar monitor USB: ./monitor_usb <host> <port>
	
	3. Ejecutar webserver: ./webserver <port>
	
	Example:
		 ./monitor_usb 127.0.0.1 8000
		 ./webserver 8000

	
	GET Request: http://<host>:9999/<requerimiento>

	

Notas:
	El programa permite monitorear los dispositivos usb segun el formato de llamadas http y json especificados en el 
	documento del proyecto.
	
	Se uso la aplicacion Postman https://www.getpostman.com/ para probar las funcionalidades.
	
	Cuando se intento correr monitor_usb como un proceso daemon, el programa dejo de funcionar correctamente. Se dio seguimiento 
	al error con escrituras en syslog y se encontro que el programa se cae en la linea 431 del archivo /src/functions.c.
	cuando se invoca al metodo getmntent de la libreria mntent que nos permite obtener una estructura para obtener informacion
	acerca de donde se monto el dispositivo.
	
	Por lo anterior mencionado no se logro que monitor_usb y webserver se ejecuten como daemon. Se debe ejecutar primero 
	monitor_usb y luego webserver ambos en dos terminales separadas.
	
	Es necesario que los dispositivos esten montados. Para asegurarse de esto se debe abrir el usb en el explorador,
	no basta que esten conectados para que se monten.
	Si el dispositivo no esta montado y esta conectado, cuando se desee acceder a el. El programa monitor_usb terminara su 
	ejecucion a modo de proteccion.
	
	Se uso la libreria Jansson para manejar la persistencia con json.
	
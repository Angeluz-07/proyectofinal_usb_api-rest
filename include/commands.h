#define N 512

/* Comandos del cliente al servidor */
#define STOP "stop\n"
#define DISCONNECT "disconnect\n"
#define LISTUSB "list usb\n"

/* Respuestas del servidor al cliente */
#define OK "OK\n"
#define QUIT "QUIT\n"
#define UNKNOWN "??\n"
#define BYE "BYE\n"
#define YES "YES\n"
#define NO "NO\n"
#define ERROR "ERROR\n"

#include <libudev.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <unistd.h>
#include <mntent.h>
#include <paths.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/resource.h>

#include <microhttpd.h>
#include <jansson.h>
/*	
	struct usb to send the usb info trough sockets as a bunch of bytes
*/
typedef struct USB{	
	char node[256];
	char idVendor[256];
	char idProduct[256];	
	char vendor[256];
	char name[256];//this is the product
	char dir[1024];//i think it refers to the dir of the usb
	struct USB *next;
}USB;


USB* createUsb(USB *head,char *node,
												 char *idVendor,
												 char *idProduct,
												 char *vendor,
												 char *name,
												 char *dir);
void showUsb(USB* head);
void freeUsbList(USB **head);

/*
		--called from monitor_usb.c
		constructs a line with all INFO OF USB DEVICES TO BE SENT TO WEBSERVER
		PROCESS. Iterate over the USB list and packs all the data up to a simple
		line.
*/
char*  getLineAllDevicesInfo(USB *head,int usbInfoLength, int nDevices);



/*
		--called from webserver.c

		get the line with all INFO OF USB DEVICES RECEIVED FROM MONITOR_USB 
		PROCESS to webserver process. Inside the line is worked, split is done 
		and stores the info in a linked list of USB struct in WEBSERVER PROCESS
		side.
*/
void setUsbListFromLine(USB **head,char *line);

void enumerar_disp_alm_masivo(struct udev* udev);

/*
	it makes the same as enumerar_disp_alm_masivo but stores the info
	in a linked list of USB struct
*/
USB* enumerar_disp_alm_masivo_toList(struct udev* udev,USB* head);

struct udev_device* obtener_hijo(struct udev* udev,
				 struct udev_device* padre,
				 const char* subsistema);
void probarAnadirArchivo(char * c);

/*
   daemonize function can be called from a program that wants to  
   initialize itself as a daemon.
   cmd parameter, we can pass the name of the .c file that calls it.
   info about this it'll be written on var/log/syslog file 
*/
void daemonize(const char *cmd);

struct mntent * getMtabStruct(struct udev_device* block);

char* getJsonString_allDevices(USB* head,json_t *root);

int getNumConnectedDevices(USB* head);
int eliminarArchivo(USB* head, char *node, char *fileName);
int anadirArchivo(USB* head, char *node, char *fileName, char *buf);
int crearScript(char *command, char *file);
int crearArchivo(char * file, char*buff);

/*
	it gives the dir of the usb device that matches with the dir(node)
	of the usb device connected
*/	
char *listarArchivos(USB* head, char *node);
void getJsonStringFromFilesInUSB(int* estado, char * direc, char *jsonString);
void getJsonStringWithError(int* estado, char *jsonString);


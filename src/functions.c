#include "./include/functions.h"
#include "./include/csapp.h"
/*
		start of functions of USB struct
*/

USB* createUsb(USB *head,char *node,
												 char *idVendor,
												 char *idProduct,
												 char *vendor,
												 char *name,
												 char *dir) { 
	
	USB* usb=(USB*)malloc(sizeof(USB));
	strcpy(usb->node,node);
	strcpy(usb->idVendor,idVendor);
	strcpy(usb->idProduct,idProduct);
	strcpy(usb->vendor,vendor);
	strcpy(usb->name,name);
	strcpy(usb->dir,dir);
	usb->next=NULL;
	if(head!=NULL){
		USB *current=head;
		while(current->next!=NULL){current=current->next;}
		current->next=usb;	
	}else{
		head=usb;
	}
	return head;
}

//to clean the USB list
void freeUsbList(USB **head) { 
		USB *current=*head;
		USB *next;
		while(current!=NULL){
			next=current->next;
			free(current);
			current=next;
		}
		*head=NULL;	
}

void showUsb(USB* head){
	USB* current=head;
	//int i=1; 	
	if(head!=NULL){
		printf("Usb connected devices : \n");
		while(current!=NULL){
			//printf("%d>> \n", i);
			printf("    Node: %s\n",current->node);
			printf("    id Vendor: %s\n",current->idVendor);	
			printf("    id Product: %s\n",current->idProduct);
			printf("    Vendor: %s\n",current->vendor);
			printf("    Name: %s\n",current->name);
			printf("    dir: %s\n\n",current->dir);
			current=current->next;
			//i++;
		}
	}else{
		printf("There aren't usb connected devices\n");
	}
}


int getNumConnectedDevices(USB* head){
		int n=0;		
		USB* current=head;
		while(current!=NULL){
			current=current->next;
			n++;
		}	
		return n;
}

char*  getLineAllDevicesInfo(USB *head, int usbInfoLength, int nDevices){
				
				//line with ALL usb's info to be sent to webserver
				char *line;
				line=(char*)malloc(usbInfoLength*nDevices);
				//clean garbage values that line
				strcpy(line,"");
				
				//code # 1
				strcpy(line,"1");

				/*
					iterate over the linked list with USB info
					so from each USB i get its info temporary in one line called
					usbInfoTemp...see below.
				*/
				char *usbInfoTemp;
				
				//+1 because i add a # at the beggining
				usbInfoTemp=(char*)malloc(usbInfoLength+1);
				strcpy(usbInfoTemp,"");

				USB* current=head;				
				while(current!=NULL){
					sprintf(usbInfoTemp,"#%s|%s|%s|%s|%s|%s",current->node,
																						 current->idVendor,	
																						 current->idProduct,
																						 current->vendor,
																						 current->name,
																						 current->dir);
					sprintf(line,"%s%s",line,usbInfoTemp);					
					current=current->next;
				}
				strcat(line,"\n");
				return line;				
}

void setUsbListFromLine(USB **head,char *line){
				char node[256];
				char idVendor[256];
				char idProduct[256];	
				char vendor[256];
				char name[256];//this is the product
				char dir[1024];
									

				//set of chars that i want to use to split the line				
				char tokchar[3]="#|\n";			

				//pointer to iterate while splitting
				char *cPtr;
				cPtr=strtok(line,tokchar);	

				while(cPtr!=NULL){															
					cPtr=strtok(NULL,tokchar);
					/*
						i check here because of the behaviour of strtok in a while loop...
						when finalize the project, please improve this.
					*/
					if(cPtr==NULL) break;		

					strcpy(node,cPtr);	
					//printf("%s\n",node);

					cPtr=strtok(NULL,tokchar);										
					strcpy(idVendor,cPtr);
					//printf("%s\n",idVendor);

					cPtr=strtok(NULL,tokchar);
					strcpy(idProduct,cPtr);																	
					//printf("%s\n",idProduct);

					cPtr=strtok(NULL,tokchar);																
					strcpy(vendor,cPtr);
					//printf("%s\n",vendor);

					cPtr=strtok(NULL,tokchar);																	
					strcpy(name,cPtr);
					//printf("%s\n",name);
	
					cPtr=strtok(NULL,tokchar);
					strcpy(dir,cPtr);
					//printf("%s\n",dir);							
					*head=createUsb(*head,node,idVendor,idProduct,vendor,name,dir);			
				}	
}


char* getJsonString_allDevices(USB* head,json_t *root){
				
				char *jsonString;
				int status=-1;
				char error[MAXLINE];
				strcpy(error, "No hay ningùn dispositivo montado");
				USB *headT=head;
				while(headT!=NULL){
					//creo un json_T temporal para pasarle cada struct usb
					json_t * json_tTemp=json_object();
					if (json_tTemp==NULL) app_error("Error to create json_object");
					//getting each parameter from usb and setting in jsonObject,
					json_object_set_new(json_tTemp, "node", json_string(headT->node));
					json_object_set_new(json_tTemp, "idVendor", json_string(headT->idVendor));
					json_object_set_new(json_tTemp, "idProduct", json_string(headT->idProduct));
					json_object_set_new(json_tTemp, "vendor", json_string(headT->vendor));
					json_object_set_new(json_tTemp, "name", json_string(headT->name));					
					json_object_set_new(json_tTemp, "dir", json_string(headT->dir));	
					//printf("%s Imprimí ",json_string_value(json_string(headT->node)));

					//then "json_array_append_new" set jsonObjectTemp to root
					json_array_append_new(root, json_tTemp);
					status=0;
					strcpy(error, "");
					headT=headT->next;
				}
				//bug for not use malloc
				json_t *object=json_object();
				if (object==NULL) app_error("Error to create json_object");
				json_object_set_new(object, "dispositivos",root);
				json_object_set_new(object,"status",json_integer(status));
				json_object_set_new(object,"str_error",json_string(error));//aqui va el mensaje de error si hubiera uno
				jsonString=json_dumps(object, JSON_INDENT(1)|JSON_PRESERVE_ORDER);
				//jsonString=json_dumps(root,JSON_INDENT(1));

				return jsonString;
}

//fix putting a sleep(0.1) on getMtabStruct call

struct udev_device* obtener_hijo(struct udev* udev,
				 struct udev_device* padre,
				 const char* subsistema){

	struct udev_device *hijo=NULL;
	struct udev_enumerate *enumerar = udev_enumerate_new(udev);
	
	udev_enumerate_add_match_parent(enumerar, padre);	
	udev_enumerate_add_match_subsystem(enumerar, subsistema);
	udev_enumerate_scan_devices(enumerar);
	
	struct udev_list_entry *dispositivos=udev_enumerate_get_list_entry(enumerar);
  	struct udev_list_entry *entrada;
	
	udev_list_entry_foreach(entrada, dispositivos) {
		const char *ruta;		
		/* Get the filename of the /sys entry for the device
		   and create a udev_device object (hijo) representing it */
		ruta= udev_list_entry_get_name(entrada);
		hijo= udev_device_new_from_syspath(udev, ruta);
		break;
	}
	/* Free the enumerator object */
	udev_enumerate_unref(enumerar);
	return hijo;
}


void enumerar_disp_alm_masivo(struct udev* udev){

	struct udev_enumerate *enumerar = udev_enumerate_new(udev);
	
	//we look for usb devices of SCSI type(MASS STORAGE)
	udev_enumerate_add_match_subsystem(enumerar, "scsi");
	udev_enumerate_add_match_property(enumerar,"DEVTYPE", "scsi_device");
	udev_enumerate_scan_devices(enumerar);

	//we get those devices
	struct udev_list_entry *dispositivos=udev_enumerate_get_list_entry(enumerar);
  	struct udev_list_entry *entrada;

	udev_list_entry_foreach(entrada, dispositivos) {
		const char *ruta;		
		/* Get the filename of the /sys entry for the device
		   and create a udev_device object (scsi) representing it */
		ruta= udev_list_entry_get_name(entrada);
		struct udev_device* scsi= udev_device_new_from_syspath(udev, ruta);

		struct udev_device* block= obtener_hijo(udev,scsi,"block");
		struct udev_device* scsi_disk= obtener_hijo(udev,scsi,"scsi_disk");

		/* In order to get information about the
		   USB device, get the parent device with the
		   subsystem/devtype pair of "usb"/"usb_device". This will
		   be several levels up the tree, but the function will find
		   it.*/
		struct udev_device* usb = udev_device_get_parent_with_subsystem_devtype(
		       scsi,
		       "usb",
		       "usb_device");		
		
		if (block && scsi_disk && usb) {		
			/* From here, we can call get_sysattr_value() for each file
				 in the device's /sys entry. The strings passed into these
				 functions (idProduct, idVendor, serial, etc.) correspond
				 directly to the files in the directory which represents
				 the USB device. */
			printf("node = %s, usb = %s:%s, scsi = %s\n",
							udev_device_get_devnode(block),
							udev_device_get_sysattr_value(usb,"idVendor"),
							udev_device_get_sysattr_value(usb, "idProduct"),
							udev_device_get_sysattr_value(scsi, "vendor"));	
			printf("  product:%s\n",udev_device_get_sysattr_value(usb,"product"));

			
			//it seems this gives the same as vendor
			//printf("  %s\n  ",udev_device_get_sysattr_value(usb,"manufacturer"));
			//for now we don't need serial
			//printf("  serial: %s\n",udev_device_get_sysattr_value(usb, "serial"));
			
		  struct mntent * mn;
			mn=getMtabStruct(block);		
			printf("  %s\n",mn->mnt_fsname);
			printf("  dir: %s\n",mn->mnt_dir);
		}
		if(block) udev_device_unref(block);
		if(scsi_disk) udev_device_unref(scsi_disk);
		if(usb) udev_device_unref(scsi_disk);
		udev_device_unref(scsi);
	}
	udev_enumerate_unref(enumerar);
}


/*
	kind of the same as enumerar_disp_alm_masivo, but return the list
	with USB structs of devices that are connected
*/
USB* enumerar_disp_alm_masivo_toList(struct udev* udev,USB* head){

	struct udev_enumerate *enumerar = udev_enumerate_new(udev);
	
	//we look for usb devices of SCSI type(MASS STORAGE)
	udev_enumerate_add_match_subsystem(enumerar, "scsi");
	udev_enumerate_add_match_property(enumerar,"DEVTYPE", "scsi_device");
	udev_enumerate_scan_devices(enumerar);

	//we get those devices
	struct udev_list_entry *dispositivos=udev_enumerate_get_list_entry(enumerar);
  	struct udev_list_entry *entrada;

	udev_list_entry_foreach(entrada, dispositivos) {
		const char *ruta;		
		/* Get the filename of the /sys entry for the device
		   and create a udev_device object (scsi) representing it */
		ruta= udev_list_entry_get_name(entrada);
		struct udev_device* scsi= udev_device_new_from_syspath(udev, ruta);

		struct udev_device* block= obtener_hijo(udev,scsi,"block");
		struct udev_device* scsi_disk= obtener_hijo(udev,scsi,"scsi_disk");

		/* In order to get information about the
		   USB device, get the parent device with the
		   subsystem/devtype pair of "usb"/"usb_device". This will
		   be several levels up the tree, but the function will find
		   it.*/
		struct udev_device* usb = udev_device_get_parent_with_subsystem_devtype(
		       scsi,
		       "usb",
		       "usb_device");		
		
		if (block && scsi_disk && usb) {		
			/* From here, we can call get_sysattr_value() for each file
				 in the device's /sys entry. The strings passed into these
				 functions (idProduct, idVendor, serial, etc.) correspond
				 directly to the files in the directory which represents
				 the USB device. */

			struct mntent * mn;
			mn=getMtabStruct(block);

			head=createUsb(head,udev_device_get_devnode(block),
							            udev_device_get_sysattr_value(usb,"idVendor"),
													udev_device_get_sysattr_value(usb, "idProduct"),
													udev_device_get_sysattr_value(scsi, "vendor"),
													udev_device_get_sysattr_value(usb,"product"),
													mn->mnt_dir);
			
			//it seems this gives the same as vendor
			//printf("  %s\n  ",udev_device_get_sysattr_value(usb,"manufacturer"));
			//for now we don't need serial
			//printf("  serial: %s\n",udev_device_get_sysattr_value(usb, "serial"));
			
		  
		}
		if(block) udev_device_unref(block);
		if(scsi_disk) udev_device_unref(scsi_disk);
		if(usb) udev_device_unref(scsi_disk);
		udev_device_unref(scsi);
	}
	udev_enumerate_unref(enumerar);
	return head;
}




/*
		function to get the mntent struct. it could be replaced
		by just getting the dir where the device is mounted, but 
		maybe we would need the struct in the future, so for now
		we use the whole struct
*/
		
struct mntent * getMtabStruct(struct udev_device* block){
			
			//file that i'll pass when i create mntent struct
			FILE * m;

			//struct to return
		  struct mntent * mn;

			/*stores the line from mtab file on system that 
				corresponds to our device*/
			char buffNewmtab[1024];

			//open the mtab file on system
			FILE *mtabFile;			
			mtabFile=Fopen("/etc/mtab","r");	

			
			/*
				this stores where is mounted, is like "/dev/dbc1" the final
			  number, it could be or not when i call udev_device_get_devnode(block).
				So, i just check the first 8 characters.
			*/
			char nodeFromDevice[20];
			strcpy(nodeFromDevice,udev_device_get_devnode(block));

			//to check line by line the mtabFile
			char linea[1024];
			
			//flag to check that found the line
			int foundLine=0;
			while(Fgets(linea, 1024, mtabFile)!=NULL){												
						if(strncmp(nodeFromDevice,linea,8)==0){						
								strcpy(buffNewmtab,linea);								
								//puts(buffNewmtab);
								foundLine=1;
						}				
			}
			if(!foundLine) app_error("device is not mounted");
	
			Fclose(mtabFile);
			

			/*
				we create a new file on the folder of our project.
				it'll contain the line with the info of our device previously
				stored on buffNewmtab. we use that to create our mtent struct
			*/
			int fdNewmtab=open("./mtab",O_CREAT|O_WRONLY|O_APPEND,S_IRWXO|S_IRWXU|S_IRWXG);

			if(write(fdNewmtab,&buffNewmtab[0],strlen(buffNewmtab))){							
							m=setmntent ("./mtab", "r");
							if(NULL==m) app_error("error creating mtent struct");							
							
							mn=getmntent(m);
							//printf("%s\n",mn->mnt_dir); 
							//printf("%s\n",mn->mnt_fsname);
							//probarAnadirArchivo(mn->mnt_dir);			
			}

			//we close it and remove it 
			close(fdNewmtab);
			remove("./mtab");

		return mn;		
}




//function to daemonize a .c program
void daemonize(const char *cmd)
{
		  int i, fd0, fd1, fd2;
    	pid_t pid;
    	struct rlimit rl;
    	//struct sigaction sa;

	     /*
	     * Clear file creation mask.
	     */
	    umask(0);

	    /*
	     * Get maximum number of file descriptors.
	     */
	    if (getrlimit(RLIMIT_NOFILE, &rl) < 0)
				unix_error("can't get file limit");
				//err_quit("%s: can't get file limit", cmd);

	    /*
	     * Become a session leader to lose controlling TTY.
	     */
	    if ((pid = fork()) < 0)
				unix_error("can't fork");
				//err_quit("%s: can't fork", cmd);
	    else if (pid != 0) /* parent */
				exit(0);
	    setsid();
	
	    /*
	     * Change the current working directory to the root so
	     * we won't prevent file systems from being unmounted.
	     */
	    if (chdir("/") < 0)
				unix_error("can't change directory");
				//err_quit("%s: can't change directory to /", cmd);

	    /*
	     * Close all open file descriptors.
	     */
	    if (rl.rlim_max == RLIM_INFINITY)
				rl.rlim_max = 1024;
	    for (i = 0; i < rl.rlim_max; i++)
				close(i);

	    /*
	     * Attach file descriptors 0, 1, and 2 to /dev/null.
	     */
	    fd0 = open("/dev/null", O_RDWR);
	    fd1 = dup(0);
	    fd2 = dup(0);

	    /*
	     * Initialize the log file.
	     */
	    openlog(cmd, LOG_CONS, LOG_DAEMON);
	    if (fd0 != 0 || fd1 != 1 || fd2 != 2) {
				syslog(LOG_ERR,"unexpected file descriptors %d %d %d",fd0,fd1,fd2);
				exit(1);
	    }	
}



// recorrer la lista hasta encontrar el que tenga el nodo igual al que le ingreso, y borrar el archivo en ese nodo
int eliminarArchivo(USB* head, char *node, char *fileName){ 
	USB *current=head;
	char command[3];
	strcpy(command,"rm");
	char file[MAXLINE];
	strcpy(file, "");
	int eliminar=-1;
	while (current!=NULL){
		if(strcmp(current->node,node)==0){
			//Usb encontrado
			//junto el nodo, con donde està montado y el nombre del archivo.
			strcat(file, current->dir);
			strcat(file,"/");
			strcat(file, fileName);
			eliminar=crearScript(command,file);
			break;				
		}
		current=current->next;
	}

	return eliminar;
}

// recorrer la lista hasta encontrar el que tenga el nodo igual al que le ingreso, y borrar el archivo en ese nodo
int anadirArchivo(USB* head, char *node, char *fileName, char *buf){ 
	USB *current=head;
	char file[MAXLINE];
	strcpy(file, "");
	int crear=-1;
	while (current!=NULL){
		if(strcmp(current->node,node)==0){ 
			//Usb encontrado. Junto el nodo, con donde està montado y el nombre del archivo.
			strcat(file, current->dir);
			strcat(file,"/");
			strcat(file, fileName);
			crear=crearArchivo(file,buf);
			break;		
		}
		current=current->next;
	}	
	return crear;
}

char *listarArchivos(USB* head, char *node){ 
	USB *current=head;
	char *dir;
	dir=(char*)calloc(MAXLINE,sizeof(char));
	while (current!=NULL){
		if(strcmp(current->node,node)==0){ 
			//Usb encontrado. Junto el nodo, con donde està montado
			crearScript("ls -a | grep -v '/$'",current->dir);
			strcpy(dir,current->dir);			
		}
		current=current->next;
	}	
	return dir;
}

void getJsonStringFromFilesInUSB(int* estado, char *direc, char *jsonString){
	json_t *archivos=json_array();							
	FILE *lista;		
	lista=Fopen("./filesInUsb.txt","r");	
	char linea[MAXLINE];
	char fileSize[MAXLINE];
	char fileName[MAXLINE];
	struct stat sbuf;
	while(Fgets(linea, MAXLINE, lista)!=NULL){
		json_t * json_tTemp=json_object();
		if (json_tTemp==NULL) app_error("Error to create json_object");
		json_object_set_new(json_tTemp, "nombre", json_string(strtok(linea,"\n")));
		strcpy(fileName, direc);
		strcat(fileName, "/");
		strcat(fileName, strtok(linea,"\n"));
		//el archivo ya no existe, sigue en el while (por si se retira el usb cuando esta listando o se elimina el archivo)
		if (stat(fileName, &sbuf) < 0) continue;					
		sprintf(fileSize, "%li",sbuf.st_size);
		strcat(fileSize," bytes");
		json_object_set_new(json_tTemp, "size", json_string(fileSize));
		json_array_append_new(archivos, json_tTemp);
		*estado=0;
	}
	Fclose(lista);
	json_t *roota=json_array();

	json_t *archivosObject=json_object();

	if (archivosObject==NULL) app_error("Error to create json_object");
	json_object_set_new(archivosObject, "archivos",archivos);
	json_array_append(roota,archivos);
	json_object_set_new(archivosObject,"status",json_integer(*estado));
	json_object_set_new(archivosObject,"str_error",json_string(""));
	strcpy(jsonString, json_dumps(archivosObject, JSON_INDENT(1)|JSON_PRESERVE_ORDER));

}

void getJsonStringWithError(int* estado, char *jsonString){
	json_t *roota=json_array();
	json_t *archivosObject=json_object();
	if (archivosObject==NULL) app_error("Error to create json_object");
	json_object_set_new(archivosObject, "archivos",json_array());
	json_object_set_new(archivosObject,"status",json_integer(*estado));
	json_object_set_new(archivosObject,"str_error",json_string("No se encuentra ningún dispositivo asociado al nodo dado"));
	strcpy(jsonString, json_dumps(archivosObject, JSON_INDENT(1)|JSON_PRESERVE_ORDER));
}

//si queremos eliminar un archivo: command=rm, file: archivo a borrar, se puede adaptar para crear otros scripts si son necesarios
int crearScript(char *command, char *file){

	char fileName[MAXLINE];//nombre del script
	//primero le añado el directorio
	strcpy(fileName,"./scripts/");
	int rfa;
	int fd;
	int fdd;
	int status;
	char fileError[50];
	char bufff[2];
	strcpy(fileError,"./error.txt");
	//le pongo el nombre del comando al archivo
	
	if (strcmp(command,"ls -a | grep -v '/$'")==0){ 
		strcat(fileName, "ls");
	}else{
		strcat(fileName, command);
	}
	//argumento necesario para la funciòn execv
	char *arg[1];
	arg[0]=NULL;
	//MAXLINE definido csapp, buff son las lìneas del script que añadirè una por una
	char buff[MAXLINE]; 
	// la primera lìnea de un script en bash, se la paso a buff
	strncpy(buff,"#!/bin/bash\n",13); 
	//elimino, si hubiera ya un archivo llamado de esa manera
	remove(fileName); 
	//creo el archivo llamado "command"
	fd=open(fileName, O_CREAT|O_WRONLY|O_APPEND,S_IRWXO|S_IRWXU|S_IRWXG);
	//escribo la primera lìnea, y asì con las siguientes 
	rfa=write(fd,buff,strlen(buff)); 
	strcpy(buff,command);
	strcat(buff," ");
	//le añado el archivo al comando, en el caso de rm: rm home/Harold/usb/filename;	
	strcat(buff,file);
	remove("./filesInUsb.txt");
	if(strcmp(command,"ls -a | grep -v '/$'")==0){
		strcpy(buff,"(ls -a ");
		strcat(buff,file);
		strcat(buff,") ");
		strcat(buff,"| grep -v '/$'");
		strcat(buff, " >>./filesInUsb.txt");	
	} 	
	rfa=write(fd,buff,strlen(buff));
	//Añado al script el manejo de errores
	strncpy(buff,"\nif [ $? -ne 0 ]; then\n",25);
	rfa=write(fd,buff,strlen(buff));
	//elimino un archivo de errores que ha sido creado antes que este con el mismo nombre.
	strncpy(buff,"echo \"-1\">> ./error.txt\n",50);
	rfa=write(fd,buff,strlen(buff));
	strcpy(buff,"fi");
	rfa=write(fd,buff,strlen(buff));		
	close(fd);
	puts(buff);
	if(rfa!=0){
		rfa=0;
		fd=0;	
	}
	//uso memoria compartida
	int *exe = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE,MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	*exe=99;
	//Se aplica concurrencia para que execv no cierre un proceso
	if(fork()==0){
		
		*exe=3;			
		//ejecuto el archivo script creado, arg: char *arg[1]; arg[0]=NULL; filename: ./scripts/rm
		*exe=execv(fileName,arg);
		puts(fileName);	
		
			
	}else{
		//esperamos que termine el otro proceso para continuar, hasta que se ejecute el execv
		wait(NULL); 
		//leo el archivo de errores que fue creado 
		fdd=open("./error.txt", O_RDONLY); 
		rfa=read(fdd,bufff,2);
		close(fdd);
		remove("./error.txt");
		puts(bufff);
		//si dio error el execv o si no se pudo ejecutar el comando se manda un error	
		if(*exe==-1 || (strcmp(bufff,"-1")==0)){
			//se puede usar en el status que se envìa a los clientes	
			status = -1;
			
		}else{
			status = 0; 
		}
	}

	munmap(exe, sizeof(int));

	return status;

}

int crearArchivo(char * file, char*buff){
	// creo el archivo nuevo,
	int crear;
	remove(file);
	int fd=open(file, O_CREAT|O_WRONLY|O_APPEND,S_IRWXO|S_IRWXU|S_IRWXG);
	char * linea;
	//corta todo el buff, por cada salto de lìnea
	linea=strtok(buff, "\n");
	char lineaS[MAXLINE];
	while(linea != NULL ){ 
		//escribo cada lìnea del archivo
		strcpy(lineaS,linea);
		strcat(lineaS, "\n");	
		write(fd,&lineaS[0],strlen(lineaS));
		linea=strtok(NULL,"\n");
	}
	crear=fd;
	close(fd);
	if (crear==-1){
		return crear;
	}else{
		return 0;	
	}
}



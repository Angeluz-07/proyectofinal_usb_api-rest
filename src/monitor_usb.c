#include "./include/functions.h"
#include "./include/commands.h"
#include "./include/csapp.h"

USB *head=NULL;

enum Estado {EXIT = 0, WAITING_COMMAND,LISTING_USB, SENDING_USB, DISCONNECTING} estado; //Estados para la FSM

/*	
   responder_cliente means that responds the client that connects
   to THIS C PROGRAM, which for our purposes it'll be the 
   webserver proccess
*/
void responder_cliente(int connfd);

int main (int argc, char **argv){

	//variables for connection with webserver.c
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;


	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	//daemonize("monitor_usb in attack mode");	

	estado = DISCONNECTING;
	listenfd = Open_listenfd(port);
	while (estado) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		syslog(LOG_INFO,"monitor_usb connected to %s (%s)\n", hp->h_name, haddrp);

		responder_cliente(connfd);
		printf("desconectando al cliente ...\n");
		Close(connfd);
	}
	printf("Bye...\n");
	exit(0);
}

void responder_cliente(int connfd)
{
	char buf[MAXLINE];
	size_t n;
	rio_t rio;

	
	/*
		+6 because there is a pipe between each string that contains
		a piece of info of USB. Example:

			/dev/sdb | 0718 | 0704 | Imation | ImationFlashDriv | /media/
			rmena/986ACEFD6ACED762				

	*/
	int usbInfoLength=sizeof(struct USB)+6;


	estado = WAITING_COMMAND;
	Rio_readinitb(&rio, connfd);
	while(estado) {
		switch(estado)
		{
			case WAITING_COMMAND:
				n = Rio_readlineb(&rio, buf, MAXLINE);

				if(n>0)
				{
					if(strcmp(buf,STOP) == 0){
						Rio_writen(connfd, QUIT, 5);
						estado = EXIT;
					} else if (strcmp(buf,DISCONNECT) == 0){
						estado = DISCONNECTING;
					} else if (strcmp(buf,LISTUSB) == 0){						
						estado = SENDING_USB;
					} else {
						Rio_writen(connfd, UNKNOWN, 3);
						puts("uknown command");
						//syslog(LOG_INFO,"Comando desconocido\n");
					}
				}else
					estado = DISCONNECTING;
				break;
			case  SENDING_USB:			
				puts("Sending usb info...");
				struct udev *udev = udev_new();
				if (!udev) {
					printf("Can't create udev\n");
					exit(1);
				}			
				head=enumerar_disp_alm_masivo_toList(udev,head);
				showUsb(head);				
				//how many devices are connected
				int nDevices=getNumConnectedDevices(head);			
				char *line=getLineAllDevicesInfo(head,usbInfoLength,nDevices);

				//SEND THE LINE WITH INFO TO WEBSERVER
				Rio_writen(connfd, line, strlen(line));

				//line was obtained with malloc inside function, so we must free it
				free(line);
				freeUsbList(&head);
				estado = WAITING_COMMAND;
				break;
			case DISCONNECTING:
				Rio_writen(connfd, BYE, 4);
				return;
				break;
			default:
				estado = DISCONNECTING;
		}
	}
}

#include "./include/csapp.h"
#include "./include/commands.h"
#include "./include/functions.h"
#include <stdbool.h>

#define INDEX "This is an Web Server, you can list usb in: http://host:port/dispositivos"
#define PORT 9999


//write on syslog and see the entries on realtime with tail -f var/log/syslog						
//syslog(LOG_INFO,"1\n");
USB *head=NULL;

struct postStatus {
    bool estado;
    char *buf;
};

static int usb_devices(void * cls,struct MHD_Connection * connection,
																	const char * url,
																	const char * method,
																	const char * version,
																	const char * upload_data,
																	size_t * upload_data_size,
		  									          void ** ptr);


/*
	remember webserver.c works as a client of monitor_usb.c
*/
enum Estado {EXIT = 0, WAITING_COMMAND, SENDING_DATA, WAITING_DATA,  LISTING_USB, DISPLAY_DATA} estado; //Estados para la FSM

int main(int argc, char **argv)
{
	int clientfd;
	char *port;
	char *host; 

	estado = WAITING_COMMAND;
	
	if (argc != 3) {
		fprintf(stderr, "usage: %s <host> <port>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];

	clientfd = Open_clientfd(host, port);

	//start the web server as daemon in defined port
	struct MHD_Daemon * d;
 	d = MHD_start_daemon(MHD_USE_THREAD_PER_CONNECTION,
		       PORT,
		       NULL,
		       NULL,
		       &usb_devices,					
		       clientfd,
		       MHD_OPTION_END);

		
	char buf[MAXLINE];
	rio_t rio;
	size_t n;
	Rio_readinitb(&rio, clientfd);
	while(estado) {
		switch(estado)
		{
			case WAITING_COMMAND:
				Fgets(buf, MAXLINE, stdin);
				Rio_writen(clientfd, buf, strlen(buf));
				n = Rio_readlineb(&rio, buf, MAXLINE);			
				Fputs(buf, stdout);
				if(strcmp(buf,OK) == 0) estado = WAITING_COMMAND;
				else if(buf[0]=='1') estado = LISTING_USB;				
				else if((strcmp(buf,BYE) == 0) || (strcmp(buf,QUIT) == 0)) estado = EXIT;
				break;			
			case LISTING_USB:					
				puts("\nproccessing line and showing info...\n");
				setUsbListFromLine(&head,buf);		
				showUsb(head);				
				freeUsbList(&head);		
				estado=WAITING_COMMAND;
				break;	
			default:
				estado = EXIT;
		}
	}
		
	//the below can be used to replace all the while above
	
	//if don't put this the programs ends as soon as starts			
	//to end just type enter
	//getchar();

	MHD_stop_daemon(d);
	Close(clientfd);
	exit(0);
}

static int usb_devices(void * cls, struct MHD_Connection * connection,
																	 const char * url,
																	 const char * method,
																	 const char * version,
																	 const char * upload_data,
																	 size_t * upload_data_size,
																	 void ** ptr) {

	static int dummy;

	//INDEX is a global variable
	const char * page = INDEX;
	struct MHD_Response * response;
	int ret;

	//if is another kind of request
	if (strcmp(method, "GET") !=0 && strcmp(method, "POST")!=0) return MHD_NO;	
	
	//if is post but url different from /archivos
	if (strcmp(method, "POST") ==0 && strcmp(url,"/archivos")!=0){	
			char c[1];
			strcpy(c,"");
			response = MHD_create_response_from_buffer (strlen(c),(void*) c,MHD_RESPMEM_PERSISTENT);
			//BAD REQUEST is error 400
			ret = MHD_queue_response(connection,MHD_HTTP_BAD_REQUEST,response);
			MHD_destroy_response(response);
			return ret;
	}

	//if is post and url:/archivos
	if(strcmp(method, "POST")==0 && strcmp(url,"/archivos")==0){
		//if(Fork()==0){//todo, al final cerramos				
			struct postStatus *post = NULL;
	  	post = (struct postStatus*)*ptr;
			if(post == NULL) {
				post = malloc(sizeof(struct postStatus));
				post->estado = false;
				*ptr = post;
			}
			if(!post->estado) {
    				post->estado = true;
			    	return MHD_YES;
			} else {
			    if(*upload_data_size != 0) {
					//cuando enviamos un dato entra aquì y obtenemos el upload_data dentro del buf
					//post->buf=NULL;
					post->buf = malloc(*upload_data_size + 1);
					strncpy(post->buf, strtok(upload_data,"}"), *upload_data_size);
					strcat(post->buf,"}");
					puts(post->buf);
					*upload_data_size = 0;
					return MHD_YES;

				  }else {
									
						json_error_t error;					
						json_t *root;
						root=json_loads(post->buf,0,&error);
										
						
						char   solicitud[MAXLINE],nodo[MAXLINE],nombre[MAXLINE],contenido[MAXLINE];
						json_t *solicitud_obj, *nodo_obj, *nombre_obj,*contenido_obj;
						

						if(root!=NULL) {
								solicitud_obj=json_object_get(root, "solicitud");
								nodo_obj=json_object_get(root, "nodo");
								nombre_obj=json_object_get(root, "nombre");			
								contenido_obj=json_object_get(root, "contenido");

								strcpy(solicitud, json_string_value(solicitud_obj));
								strcpy(nodo, json_string_value(nodo_obj));
																
								strcpy(nombre, nombre_obj!=NULL ? json_string_value(nombre_obj):"");
								strcpy(contenido,contenido_obj!=NULL ? json_string_value(contenido_obj) : "");
						}
						puts("1");
						//if (solicitud_obj!=NULL) json_decref(solicitud_obj);
						puts("2");
						//if (nodo_obj!=NULL) json_decref(nodo_obj);
						puts("3");						
						if (nombre_obj!=NULL) json_decref(nombre_obj);
						puts("4");
						if (contenido_obj!=NULL) json_decref(contenido_obj);
						puts("5");
						puts(solicitud);
						puts(nodo);

						//solicitud invalida
						if(strcmp(solicitud,"borrar")!=0 && strcmp(solicitud,"crear")!=0 && strcmp(solicitud,"listar")!=0){
							char c[1];
							strcpy(c,"");
							response = MHD_create_response_from_buffer (strlen(c),(void*) c,MHD_RESPMEM_MUST_COPY);							
							ret = MHD_queue_response(connection,MHD_HTTP_BAD_REQUEST,response);
							MHD_destroy_response(response);
							return ret;							
						}
						
						
						int clientfd=cls;							
						char buf[MAXLINE];
						rio_t rio;
						size_t n;
						Rio_readinitb(&rio, clientfd);

						strcpy(buf,LISTUSB);
						Rio_writen(clientfd,buf, strlen(buf));
						n = Rio_readlineb(&rio, buf, MAXLINE);	
						//Fputs(buf, stdout);						
						
						setUsbListFromLine(&head,buf);

						//if(buf[0]=='1')
					
						printf("Solicitud: %s\n Nodo: %s\n Nombre: %s\n Contenido: %s\n",solicitud,nodo,nombre,contenido);						
						char xresponse[MAXLINE];
						unsigned int xcode;
						strcpy(xresponse,"");//Esta linea te faltaba
						xcode=MHD_HTTP_BAD_REQUEST;
						
						switch(solicitud[0]){							
							case 'l':
								puts("listando archivos...");
								int statusListar=-1;
								char direc[MAXLINE];								
								char jsonString[MAXLINE];													
								strcpy(direc,listarArchivos(head,nodo));
								if(strlen(direc)>0){
									getJsonStringFromFilesInUSB(&statusListar, direc, jsonString);
									strcpy(xresponse,jsonString);
									xcode=MHD_HTTP_OK;
									//free(jsonString);	
								}else{
									getJsonStringWithError(&statusListar, jsonString);
									strcpy(xresponse,jsonString);
									xcode=MHD_HTTP_OK;									
								}														
								break;							
							case 'c':	
								puts("creando archivo...");
								int creo=anadirArchivo(head, nodo, nombre, contenido);							
								switch(creo){
									case 0:
										xcode=MHD_HTTP_OK;
										break;
									case -1:
										strcpy(xresponse, "ERROR");
										xcode=MHD_HTTP_INTERNAL_SERVER_ERROR;//Codigo 500 internal server error
										break;							
								}								
								break;						
							case 'b':								
								//borrar
								puts("borrando archivo...");
								int elimino=eliminarArchivo(head,nodo,nombre);
								switch(elimino){
									case 0:
										xcode=MHD_HTTP_OK;
										break;
									case -1:
										strcpy(xresponse, "Archivo no Encontrado");
										xcode=MHD_HTTP_NOT_FOUND;
										break;									
								}													
						}	
						freeUsbList(&head);					
						puts("free1");													
						//if(post->buf!=NULL) free(post->buf);
						puts("free2");
						puts(xresponse);
						response = MHD_create_response_from_buffer (strlen(xresponse),(void*) xresponse,MHD_RESPMEM_MUST_COPY);
						
						ret = MHD_queue_response(connection,xcode,response);
						MHD_destroy_response(response);											
						return ret;			
					 }
				}
				if(post != NULL) free(post);		
	}			

	/* unexpected method */
	if (&dummy != *ptr)
	{
	    /* The first time only the headers are valid,
		do not respond in the first round... */
	    *ptr = &dummy;
	    return MHD_YES;
	}
	if (0 != *upload_data_size)
	   return MHD_NO; 
	*ptr = NULL; /* clear context pointer */


	if(strcmp(url,"/dispositivos")==0){ 											
				json_t *root = json_array();
				int clientfd=cls;		
				char buf[MAXLINE];
				rio_t rio;
				Rio_readinitb(&rio, clientfd);	
				//sends "list usb\n" directly. W
				strcpy(buf,LISTUSB);
				Rio_writen(clientfd,buf, strlen(buf));
			  Rio_readlineb(&rio, buf, MAXLINE);	
				Fputs(buf, stdout);
				if(buf[0]=='1'){			
					puts("\nproccessing line and showing info...\n");
					setUsbListFromLine(&head,buf);
					showUsb(head);	
					char *jsonString=getJsonString_allDevices(head,root);						
					freeUsbList(&head);	
					response = MHD_create_response_from_buffer (strlen(jsonString),(void*) jsonString,MHD_RESPMEM_MUST_COPY);
					//"jsonString not allocated works, but must be improved"								
					json_decref(root);
				}
	}else{
		response=MHD_create_response_from_buffer(strlen(page), (void*)page, MHD_RESPMEM_MUST_COPY);
	}	
	ret = MHD_queue_response(connection,MHD_HTTP_OK,response);
	MHD_destroy_response(response);
	return ret;
}


